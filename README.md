# Advent of Code 2020

My solutions and working files for the 2020 run of Advent of Code.

https://adventofcode.com/2020

## For Other Competitors

Obviously, simply looking at my solutions will teach you very little. To this purpose, I will attempt to keep a running list of notes or hints either in this file, or linked from this file, for each day's solution. My language of choice of is Python, so my solutions and notes will be focused on Python approaches. I've also generously sprinkled notes throughout my solutionsfor items that newer Pythonistas may not have seen before.

I would suggest attempting the day's challenge on your own first. If you are stuck, then check for that day's notes below. Only once you have a working solution would I look at my code.

If the .ipynb files are not displayed with syntax highlighting, go to [Settings -> Preferences](https://gitlab.com/-/profile/preferences) and change the Syntax highlighting theme to "White".

### Day 1

This is one of those classic Python conundrums: where the solution is 90% knowing the standard libraries.

As always, when dealing with combinations of items, or advanced looping, we look to the [itertools](https://docs.python.org/3/library/itertools.html) library. Specifically, the `permutations(iterable, r=None)` method will provide a simple way to get each possible *r*-item combination of *iterable*.

For a generic solution, the [functools](https://docs.python.org/3/library/functools.html) library's `reduce(function, iterable)` method is also valuable.

### Day 2

Again, this solution is made much easier with some knowledge of the Python standard library. One of the most valuable libraries: [collections](https://docs.python.org/3/library/collections.html) has the very useful `Counter` class which automatically counts the number of occurances of an item in an iterable. As strings are iterables, `Counter` is an easy way to count the number of characters in a string.

The rest of this solution is mostly parsing the input data--which I imagine will be a larger and larger part of the problems as we progress. If this was a timed or resource-limited problem, I probably would have used a regular expression to parse each line, however, as we can take as long as we want with whatever resources we have, a few simple repeated applications of `str.split()` along with an understanding of string subscription make this process quick and easy.

The use of the new `dataclass` makes returning a list of parsed lines easy. However, most solutions won't need to parse and count in different steps, so this is more of an optional feature.

For Part B, we simply use our parsed data in a different way and re-write our test for success. Knowledge of the lesser-known XOR operator makes things easier too.